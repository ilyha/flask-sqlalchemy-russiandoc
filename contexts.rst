.. _contexts:

.. currentmodule:: flask_sqlalchemy

Введение в Contexts
===================

Если вы планируете использовать только одно приложения, тогда можете
пропустить эту статью.  Просто передайте приложение в конструктор
:class:`SQLAlchemy` и можно начинать работать.  Однако, если вы хотите
использовать больше чем одно приложение или создавать их динамически в
функции, тогда вам следует прочитать этот текст.

Если вы объявляете ваше приложени в функции, но объект :class:`SQLAlchemy`
глобальны, как потом узнать об объекте создателе?  Ответ  - функция
:meth:`~SQLAlchemy.init_app`::

    from flask import Flask
    from flask_sqlalchemy import SQLAlchemy

    db = SQLAlchemy()

    def create_app():
        app = Flask(__name__)
        db.init_app(app)
        return app


Что она делает - подготавливает ваше приложение для работы с
:class:`SQLAlchemy`. Однако при этом не связывает обект :class:`SQLAlchemy`
с вашим приложением.  Почему бы этого не сделать? Потому что возможно
использование больше чем одно приложение.

Как же теперь :class:`SQLAlchemy` узнает о вашем приложении?
Вам необходимо установить контекст приложения.  Если вы работаете
внутри функции Flask, это происходит автоматически. Однако если вы работаете
в интерактивной оболочке - вам придется сделать это вручную
(смотрите `Creating an Application Context
<http://flask.pocoo.org/docs/appcontext/#creating-an-application-context>`_).

В двух словах, сделать так:

>>> from yourapp import create_app
>>> app = create_app()
>>> app.app_context().push()

Внутри функции также можно использовать конструкцию with::

    def my_function():
        with app.app_context():
            user = db.User(...)
            db.session.add(user)
            db.session.commit()

Некоторые функции внутри Flask-SQLAlchemy также могут принимать объект
приложения:

>>> from yourapp import db, create_app
>>> db.create_all(app=create_app())

`Оригинал этой страницы <http://flask-sqlalchemy.pocoo.org/2.1/contexts/>`_