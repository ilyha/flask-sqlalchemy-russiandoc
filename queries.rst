.. _queries:

.. currentmodule:: flask_sqlalchemy

Select, Insert, Delete
======================

Теперь, когда вы :ref:`объявили модели <models>` пришло время для запроса
данных из базы. Мы будем использовать модель определеннкю в глвае
:ref:`quickstart`.

Вставка записей
---------------

Прежде чем запросить какие либо данные, мы должны вставить эти данные в
базу данных.  Все ваши модели должны иметь конструктор, проверьте, что
вы не забыли этого сделать. Конструкторы используются только вами,
поэтому все зависит от того как вы их определите.

Вставка данных в базу данных проходит в три этапа:

1.  Создание объекта Python
2.  Добавление его в сессию
3.  Commit сессии

Сессия здесь это не Flask сессия, а Flask-SQLAlchemy.
По сути это расширенная версия транзакции базы данных.
Как это работает:

>>> from yourapp import User
>>> me = User('admin', 'admin@example.com')
>>> db.session.add(me)
>>> db.session.commit()

Отлично, это не сложно.  Что здесь произошло?  Перед тем как вы добавили
объект в сессию, SQLAlchemy не планировал добавлять его в транзакцию.
Это хорошо, потому что вы можете на данном этапе отменить изменения.
Для примера представьте создание поста настранице, но вы только хотели
посмотреть как будет выглядит пост в предварительном просмотре вместо
сохраниения его в базу данных.

Вызов Функции :func:`~sqlalchemy.orm.session.Session.add` добавляет
объект. Она будет вызвать оператор  `INSERT` для базы данных, но так как
транзакция еще не завершена, вы не получаете идентификатор.
Если вы выполните commit, ваш пользователь получет ID:

>>> me.id
1

Удаление записей
----------------

Удаление записей очень простое, вместо
:func:`~sqlalchemy.orm.session.Session.add` используйте
:func:`~sqlalchemy.orm.session.Session.delete`:

>>> db.session.delete(me)
>>> db.session.commit()

Запрос записей
--------------

Итак, как же нам получить данные обратно из нашей базы данных?
Для этой цели Flask-SQLAlchemy предоставляет атрибут :attr:`~Model.query`
для класса :class:`Model`.  Когда вы обращаетесь к нему вы получаете
новый объект запроса по всем записям.  Вы можете использовать метод
:func:`~sqlalchemy.orm.query.Query.filter` для фильтрации записей перед
выполнением выборки :func:`~sqlalchemy.orm.query.Query.all` или
:func:`~sqlalchemy.orm.query.Query.first`.  Если вы хотите произвести
выборку по первичном ключу используйте
:func:`~sqlalchemy.orm.query.Query.get`.

Рассмотрим несколько запросов для следующих записей в базе данных:

=========== =========== =====================
`id`        `username`  `email`
1           admin       admin@example.com
2           peter       peter@example.org
3           guest       guest@example.com
=========== =========== =====================

Получить пользователя по имени:

>>> peter = User.query.filter_by(username='peter').first()
>>> peter.id
2
>>> peter.email
u'peter@example.org'

То же, что и выше, но для не существующего пользователя вернуть
`None`:

>>> missing = User.query.filter_by(username='missing').first()
>>> missing is None
True

Выбор нескольких пользователей, более сложным выражением:

>>> User.query.filter(User.email.endswith('@example.com')).all()
[<User u'admin'>, <User u'guest'>]

Упорядочить по какому либо полю:

>>> User.query.order_by(User.username).all()
[<User u'admin'>, <User u'guest'>, <User u'peter'>]

Установить лимит на количество возврщаемых значений:

>>> User.query.limit(1).all()
[<User u'admin'>]

Получить пользователя по первичному ключу:

>>> User.query.get(1)
<User u'admin'>


Запросы и представления
-----------------------

Если вы создаете функции представлений во Flask, часто бывает удобно
вернуть 404 ошибку для отсутствующих данных. Т.к. это очень
распространненая идома, Flask-SQLAlchemy предоставляет инструменты
для данных целей. Вместо
:meth:`~sqlalchemy.orm.query.Query.get` можно использовать
:meth:`~Query.get_or_404` и вместо
:meth:`~sqlalchemy.orm.query.Query.first` :meth:`~Query.first_or_404`.
Они будут вызывать исключение с номером 404 вместо возвращения
знаения `None`::

    @app.route('/user/<username>')
    def show_user(username):
        user = User.query.filter_by(username=username).first_or_404()
        return render_template('show_user.html', user=user)

`Оригинал этой страницы <http://flask-sqlalchemy.pocoo.org/2.1/queries/>`_