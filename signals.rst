.. _signals:

.. currentmodule:: flask_sqlalchemy

Поддержка сигналов
==================

Подключение к следующим сигналам позволяет принимать уведомления до и после
применения изменений в базе данных.
Эти изменения отслеживаются только если  ``SQLALCHEMY_TRACK_MODIFICATIONS``
включен в конфигурации.

.. versionadded:: 0.10
.. versionchanged:: 2.1
   ``before_models_committed`` срабатывает корректно.
.. deprecated:: 2.1
   Будет отключена по умолчанию в будущих версиях.

.. data:: models_committed

   Этот сигнал посылается, когда измененные модели были зафиксированы в базе данных.

   Отправителем является приложение которое запустило изменение.
   Принимающий получает параметр ``changes`` со списком кортежей в форме
   ``(model instance, operation)``.

   Инициирующие операции  ``'insert'``, ``'update'``, и ``'delete'``.

.. data:: before_models_committed

   Этот сигнал работает точно так же, как  :data:`models_committed`
   но запускается перед применением изменений.

`Оригинал этой страницы <http://flask-sqlalchemy.pocoo.org/2.1/signals/>`_