.. _quickstart:

Быстрый старт
=============

.. currentmodule:: flask_sqlalchemy

Flask-SQLAlchemy прост в использовании, удобен в простых приложениях, и
легко расширяется для сложных.  Для получения дополнительных сведений,
просмотрите API докупентацию  класса :class:`SQLAlchemy`.

Минимальное приложение
----------------------

Для общего случая с одним приложением Flask все что вам необходимо сделать
это создать ваше Flask приложение, загрузить конфигурацию с выбором БД,
затем создать объект :class:`SQLAlchemy` передав ему наше Flask приложение.

После создания, этот объект содержит все функции и инструменты из
:mod:`sqlalchemy` и :mod:`sqlalchemy.orm`. Кроме того он предоставляет
класс  ``Model`` который может быть использован для описания модели::

    from flask import Flask
    from flask_sqlalchemy import SQLAlchemy

    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
    db = SQLAlchemy(app)


    class User(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        username = db.Column(db.String(80), unique=True)
        email = db.Column(db.String(120), unique=True)

        def __init__(self, username, email):
            self.username = username
            self.email = email

        def __repr__(self):
            return '<User %r>' % self.username

Для создания базы данных, просто импортируйте объект `db` из
Python shell и запустите
:meth:`SQLAlchemy.create_all` метод для создания таблиц и базы данных::

    >>> from yourapplication import db
    >>> db.create_all()

Бах, и база данных создана.  Теперь создадим нескольких пользователей::

    >>> from yourapplication import User
    >>> admin = User('admin', 'admin@example.com')
    >>> guest = User('guest', 'guest@example.com')

Но они не будут занесены в базу данных пока мы не выполним следующее::

    >>> db.session.add(admin)
    >>> db.session.add(guest)
    >>> db.session.commit()

Доступ к данным в базе данных очень прост::

    >>> User.query.all()
    [<User u'admin'>, <User u'guest'>]
    >>> User.query.filter_by(username='admin').first()
    <User u'admin'>

Простые связи
-----------------

SQLAlchemy подключается к реляционным базам данных, и что в них действительно
хорошо - это связи. Как пример, рассмотрим приложение использующее две
таблицы, которые имеют свзяи друг с другом::

    from datetime import datetime


    class Post(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        title = db.Column(db.String(80))
        body = db.Column(db.Text)
        pub_date = db.Column(db.DateTime)

        category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
        category = db.relationship('Category',
            backref=db.backref('posts', lazy='dynamic'))

        def __init__(self, title, body, category, pub_date=None):
            self.title = title
            self.body = body
            if pub_date is None:
                pub_date = datetime.utcnow()
            self.pub_date = pub_date
            self.category = category

        def __repr__(self):
            return '<Post %r>' % self.title


    class Category(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(50))

        def __init__(self, name):
            self.name = name

        def __repr__(self):
            return '<Category %r>' % self.name

Для начала созданим некоторые объекты::

    >>> py = Category('Python')
    >>> p = Post('Hello Python!', 'Python is pretty cool', py)
    >>> db.session.add(py)
    >>> db.session.add(p)

Теперь т.к. мы обяъвили ``posts`` как объект связи со свойством dynamic  в backref,
он отображается как запрос(прим. пер. не смог сформулировать правильно)::

    >>> py.posts
    <sqlalchemy.orm.dynamic.AppenderBaseQuery object at 0x1027d37d0>

Он ведет себя как обычный объект запроса, так мы можем выполнить запрос
на все сообщения, которые связаны с нашей тестовой категорией "Python"::

    >>> py.posts.all()
    [<Post 'Hello Python!'>]


Дорога к просвящению
---------------------

Главное, что неоходимо знать, по сравнению с обычной SQLAlchemy:

1.  :class:`SQLAlchemy` дает вам доступ к следующим вещам:

    -   все функции и классы из :mod:`sqlalchemy` и
        :mod:`sqlalchemy.orm`
    -   предсконфигурированный с ограниченной областью видимости объект сессии называемый ``session``
    -    :attr:`~SQLAlchemy.metadata`
    -    :attr:`~SQLAlchemy.engine`
    -   методы :meth:`SQLAlchemy.create_all` и :meth:`SQLAlchemy.drop_all`
        для создания и удаления таблиц в соответсвии с моделями.
    -   Базовый класс :class:`Model` являющийся конфигурационной декларативной основой.

2.  Класс :class:`Model` декларативный базовый класс ведет себя как обычный
    класс Python, но имеет атрибут ``query``, который может быть использован
    для запроса к модели.  (:class:`Model` и :class:`BaseQuery`)

3.  Вы должны выполнить commit сессию, но не должны удалять ее в конце запроса,
    Flask-SQLAlchemy сделает это за вас.

`Оригинал этой страницы <http://flask-sqlalchemy.pocoo.org/2.1/quickstart/>`_