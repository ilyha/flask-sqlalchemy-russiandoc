.. _config:

.. currentmodule:: flask_sqlalchemy

Конфигурация
============

Здесь представлены значения конфигурации используемые в Flask-SQLAlchemy.
Flask-SQLAlchemy загружает эти значения из вашей конфигурации Flask, которая
может быть заданна несколькими путями. Стоит обратить внимание, что некоторые
значения не могут быть изменены после создания экземпляра, поэтому убедитесь
что они заданны как можно раньше и не меняйте их во время выполнения приложения.


Конфигурационные ключи
----------------------

Список конфигурационных ключей currently understood by the extension:

.. tabularcolumns:: |p{6.5cm}|p{8.5cm}|

================================== =========================================
``SQLALCHEMY_DATABASE_URI``        Путь/URI базы данных, который будет 
                                   использоваться для подключения. Пример:

                                   - ``sqlite:////tmp/test.db``
                                   - ``mysql://username:password@server/db``
``SQLALCHEMY_BINDS``               A dictionary that maps bind keys to
                                   SQLAlchemy connection URIs.  For more
                                   information about binds see :ref:`binds`.
``SQLALCHEMY_ECHO``                Если установлен в `ёTrue`ё то SQLAlchemy 
                                   будет выводить все сообщения в stderr, 
                                   что может быть полезно при отладке.
``SQLALCHEMY_RECORD_QUERIES``      Может применяться для явного отключения 
                                   или включения записи запросов. Запись 
                                   запросов происходит автоматически в 
                                   режимах debug и testing. Для 
                                   дополнительной информации смотрите 
                                   :func:`get_debug_queries`
``SQLALCHEMY_NATIVE_UNICODE``      Может использоваться для отключения 
                                   встроенной поддержки юникода. This is 
                                   required for some database adapters (like
                                   PostgreSQL on some Ubuntu versions) when 
                                   used with improper database defaults that
                                   specify encoding-less databases.
``SQLALCHEMY_POOL_SIZE``           The size of the database pool.  Defaults
                                   to the engine's default (usually 5)
``SQLALCHEMY_POOL_TIMEOUT``        Specifies the connection timeout for the
                                   pool.  Defaults to 10.
``SQLALCHEMY_POOL_RECYCLE``        Количество секунд по истечению которого 
                                   соединение автоматически перезапустится.
                                   Это необходимо для MySQL, которая по 
                                   умолчанию удаляет соединения после 8 
                                   часов простоя. Если используется MySQL
                                   Flask-SQLAlchemy автоматически 
                                   устанавливает его равным 2 часам.
``SQLALCHEMY_MAX_OVERFLOW``        Контролирует количество соединений 
                                   которые могут быть созданы после того 
                                   как pool набрал свой максимальный размер.
                                   When those additional connections are 
                                   returned to the pool, they are 
                                   disconnected and discarded.
``SQLALCHEMY_TRACK_MODIFICATIONS`` Если установлен в ``True``, то  
                                   Flask-SQLAlchemy будет отслеживать 
                                   изменения объектов и посылать сигналы.
                                   По умолчанию становлен в ``None``, что
                                   включает отслеживание но выводит 
                                   предупреждение, что в будующем будет
                                   отчключена по умолчанию. Данная функция 
                                   требует дополнительную память, и должна 
                                   быть отключена если не используется.
================================== =========================================

.. versionadded:: 0.8
   Были добавлены конфигурационные ключи ``SQLALCHEMY_NATIVE_UNICODE``,
   ``SQLALCHEMY_POOL_SIZE``,    ``SQLALCHEMY_POOL_TIMEOUT`` и
   ``SQLALCHEMY_POOL_RECYCLE``.

.. versionadded:: 0.12
   Был добавлен конфигурационный ключ ``SQLALCHEMY_BINDS``.

.. versionadded:: 0.17
   Был добавлен конфигурационный ключ  ``SQLALCHEMY_MAX_OVERFLOW``.

.. versionadded:: 2.0
   Был добавлен конфигурационный ключ   ``SQLALCHEMY_TRACK_MODIFICATIONS``.
.. versionchanged:: 2.1
   ``SQLALCHEMY_TRACK_MODIFICATIONS`` выводит предупреждение если не установлен.

Формат connection URI
---------------------

Для получения полного списка поддерживаемых connection URIs обратитесь к
документации SQLAlchemy (`Supported Databases
<http://www.sqlalchemy.org/docs/core/engines.html>`_). Здесь показаны
некоторые общие строки подключения.

SQLAlchemy указывает источник БД (прим. пер. Engine буду называть БД)
как URI комбинированный с опциональными ключевыми словами определяющими
параметры БД. Формат записи URI::

    dialect+driver://username:password@host:port/database

Многие части в строке - необязательны.  Если driver не указан то
выбирает один из стандартных (убедитесь что *не* включили ``+`` в этом
случае).

Postgres::

    postgresql://scott:tiger@localhost/mydatabase

MySQL::

    mysql://scott:tiger@localhost/mydatabase

Oracle::

    oracle://scott:tiger@127.0.0.1:1521/sidname

SQLite (обратите внимание на четыре слеша)::

    sqlite:////absolute/path/to/foo.db

Использование специальных MetaData и соглашение об именовании
-------------------------------------------------------------

Вы можете построить объект :class:`SQLAlchemy`со специальным объектом
:class:`~sqlalchemy.schema.MetaData`.
Это позволяет вам, помимо всего прочего,
задать `собственное ограничение именования.
<http://docs.sqlalchemy.org/en/latest/core/constraints.html#constraint-naming-conventions>`_
Это имеет важное значение для работы с миграциями баз данных (например с использованием
`alembic <https://alembic.readthedocs.org>`_ как указанно
`здесь <http://alembic.readthedocs.org/en/latest/naming.html>`_.
Т.к. SQL не определяет соглашений по именованию, нет гарантий
совместимости по умолчанию среди реализаций базы данных.
Вы можете определить пользовательские соглашения об именах,
как это, как это было предложено в документации SQLAlchemy::

    from sqlalchemy import MetaData
    from flask import Flask
    from flask_sqlalchemy import SQLAlchemy

    convention = {
        "ix": 'ix_%(column_0_label)s',
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s"
    }

    metadata = MetaData(naming_convention=convention)
    db = SQLAlchemy(app, metadata=metadata)

Для дополнительной информации о классе :class:`~sqlalchemy.schema.MetaData`,
`обратитесь к официальной документации
<http://docs.sqlalchemy.org/en/latest/core/metadata.html>`_.

`Оригинал этой страницы <http://flask-sqlalchemy.pocoo.org/2.1/config/>`_